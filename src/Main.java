import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import models.RawModel;
import models.TexturedModel;
import net.java.games.input.Keyboard;
import objConverter.ModelData;
import objConverter.OBJFileLoader;
import particles.Particle;
import particles.ParticleMaster;
import particles.ParticleSystem;

import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;
import terrains.Terrain;
import textures.ModelTexture;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import water.Water;
import water.WaterEntity;
import entities.Camera;
import entities.Entity;
import entities.Light;
import entities.MaterialLightReflectionType;
import entities.Player;

public class Main {

	public static void main(String[] args) {
		DisplayManager.createDisplay();

		Loader loader = new Loader();

		TerrainTexture terrainBackgroundTexture = new TerrainTexture(loader.loadTexture("grass"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("mud"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));

		TerrainTexturePack texturePack = new TerrainTexturePack(terrainBackgroundTexture, rTexture, gTexture, bTexture);
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));

		ModelData playerModelData = OBJFileLoader.loadOBJ("player");
		ModelData treeModelData = OBJFileLoader.loadOBJ("tree1");
		ModelData fernModelData = OBJFileLoader.loadOBJ("fern");
		ModelData grassModelData = OBJFileLoader.loadOBJ("grass");
		ModelData flowerModelData = OBJFileLoader.loadOBJ("grassModel");
		ModelData lowPolyTreeModelData = OBJFileLoader.loadOBJ("tree2");

		RawModel playerModel = loader.loadToVAO(playerModelData);
		RawModel treeModel = loader.loadToVAO(treeModelData);
		RawModel fernModel = loader.loadToVAO(fernModelData);
		RawModel grassModel = loader.loadToVAO(grassModelData);
		RawModel flowerModel = loader.loadToVAO(flowerModelData);
		RawModel lowPolyTreeModel = loader.loadToVAO(lowPolyTreeModelData);

		TexturedModel staticPlayerModel = new TexturedModel(playerModel, new ModelTexture(loader.loadTexture("player"), MaterialLightReflectionType.CHROME));
		TexturedModel staticTreeModel = new TexturedModel(treeModel, new ModelTexture(loader.loadTexture("tree1"), MaterialLightReflectionType.ALUMINIUM));
		TexturedModel staticFernModel = new TexturedModel(fernModel, new ModelTexture(loader.loadTexture("fern"), MaterialLightReflectionType.ALUMINIUM));
		TexturedModel staticGrassModel = new TexturedModel(grassModel, new ModelTexture(loader.loadTexture("petal"), MaterialLightReflectionType.ALUMINIUM));
		TexturedModel staticFlowerModel = new TexturedModel(flowerModel, new ModelTexture(loader.loadTexture("flower"), MaterialLightReflectionType.ALUMINIUM));
		TexturedModel staticLowPolyTreeModel = new TexturedModel(lowPolyTreeModel, new ModelTexture(loader.loadTexture("tree2"), MaterialLightReflectionType.ALUMINIUM));
		Water staticWaterModel = new Water(0, -1, loader, new ModelTexture(loader.loadTexture("water"), MaterialLightReflectionType.COOPER));
		
		List<Entity> entities = new ArrayList<>();

		Player player = new Player(staticPlayerModel, new Vector3f(400, 0, -300), 0, 0, 0, 0.2f);
		Terrain terrain = new Terrain(0, -1, loader, texturePack, blendMap);
	
		WaterEntity waterEntity = new WaterEntity(new TexturedModel(staticWaterModel.getModel(), staticWaterModel.getTexture()), new Vector3f(staticWaterModel.getX(), WaterEntity.ZERO_LEVEL, staticWaterModel.getZ()), 0, 0, 0, 1);

		Random random = new Random();
		for (int i = 0; i < 200; i++) {
			float x = random.nextFloat() * Terrain.SIZE;
			float z = random.nextFloat() * -Terrain.SIZE;
			float y = terrain.getHeightOfTerrain(x, z);
			float scale = Math.min(Math.max(random.nextFloat(), 0.1f), 0.4f);
			entities.add(new Entity(staticTreeModel, new Vector3f(x, y - 1, z), 0, random.nextInt(100), 0, scale));
		}

		for (int i = 0; i < 100; i++) {
			float x = random.nextFloat() * Terrain.SIZE;
			float z = random.nextFloat() * -Terrain.SIZE;
			float y = terrain.getHeightOfTerrain(x, z);
			float scale = Math.min(Math.max(random.nextFloat(), 0.15f), 0.3f);
			entities.add(new Entity(staticLowPolyTreeModel, new Vector3f(x, y - 4, z), 0, random.nextInt(100), 0, scale));
		}

		for (int i = 0; i < 400; i++) {
			float x = random.nextFloat() * Terrain.SIZE;
			float z = random.nextFloat() * -Terrain.SIZE;
			float y = terrain.getHeightOfTerrain(x, z);
			entities.add(new Entity(staticFernModel, new Vector3f(x, y, z), 0, 0, 0, 2));
		}

		for (int i = 0; i < 800; i++) {
			float x = random.nextFloat() * Terrain.SIZE;
			float z = random.nextFloat() * -Terrain.SIZE;
			float y = terrain.getHeightOfTerrain(x, z);
			entities.add(new Entity(staticGrassModel, new Vector3f(x, y, z), 0, 0, 0, 0.2f));
		}

		for (int i = 0; i < 1000; i++) {
			float x = random.nextFloat() * Terrain.SIZE;
			float z = random.nextFloat() * -Terrain.SIZE;
			float y = terrain.getHeightOfTerrain(x, z);
			y = terrain.getHeightOfTerrain(x, z);
			entities.add(new Entity(staticFlowerModel, new Vector3f(x, y, z), 0, 0, 0, 5));
		}

		Light light = new Light(new Vector3f(2000, 2000, 2000), new Vector3f(1, 1, 1));
		List<Light> lights = new ArrayList<>();
		lights.add(light);
		lights.add(new Light(new Vector3f(185, 10, -293), new Vector3f(2, 0, 0), new Vector3f(1, 0.01f, 0.002f)));

		Camera camera = new Camera(player);
		MasterRenderer renderer = new MasterRenderer(loader);
		ParticleMaster.init(loader, renderer.getProjectionMatrix());

		while (!Display.isCloseRequested()) {
			camera.move();
			player.move(terrain, waterEntity);
			waterEntity.move();
			
			ParticleMaster.update();
			
			renderer.processTerrain(terrain);
			renderer.processEntity(player);
			renderer.processWater(waterEntity);

			for (Entity entity : entities)
				renderer.processEntity(entity);

			renderer.render(lights, camera);
			
			ParticleMaster.renderParticles(camera);

			DisplayManager.updateDisplay();
		}

		ParticleMaster.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();

	}

}