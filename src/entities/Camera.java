package entities;
 
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

import net.java.games.input.Mouse;
 
public class Camera {
	
	private float distanceFromPlayer = 50;
	private float angleAroundPlayer = 0;
     
    private Vector3f position = new Vector3f(400, 20, -300);
    private float pitch = 20;
    private float yaw = 0;
    private float roll;
    private float speed = 0.5f;
    
    private Player player;
     
    public Camera(Player player){
    	this.player = player;
    }
     
    public void move(){
        calculateZoom();
        calculatePitch();
        calculateAngleAroundPlayer();
        
        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance();
        
        calculateCameraPosition(horizontalDistance, verticalDistance);
        
        this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
    }
 
    public Vector3f getPosition() {
        return position;
    }
 
    public float getPitch() {
        return pitch;
    }
 
    public float getYaw() {
        return yaw;
    }
 
    public float getRoll() {
        return roll;
    }
    
    private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {
    	float theta = player.getRotY() + angleAroundPlayer;
    	float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
    	float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));
    	
    	position.x = player.getPosition().x - offsetX;
    	position.z = player.getPosition().z - offsetZ;
    	position.y = verticalDistance + player.getPosition().y;
    }
    
    private float calculateHorizontalDistance() {
    	return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }
    
    private float calculateVerticalDistance() {
    	return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }
    
    private void calculateZoom() {
    	float zoomLevel = org.lwjgl.input.Mouse.getDWheel() * 0.1f;
    	distanceFromPlayer -= zoomLevel;
    }
    
    private void calculatePitch() {
    	if(org.lwjgl.input.Mouse.isButtonDown(1)) {
    		float pitchChange = org.lwjgl.input.Mouse.getDY() * 0.1f;
    		pitch -= pitchChange;
    	}
    }
    
    private void calculateAngleAroundPlayer() {
    	if(org.lwjgl.input.Mouse.isButtonDown(0)) {
    		float angleChange = org.lwjgl.input.Mouse.getDX() * 0.3f;
    		angleAroundPlayer -= angleChange;
    	}
    }
    
}