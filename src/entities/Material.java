package entities;

public class Material {
	private float shineDamper;
    private float reflectivity;
    
    public Material() {
    	this(MaterialLightReflectionType.DEFAULT);
    }
    
    public Material(MaterialLightReflectionType materialType) {
    	this.getMatarialAttributes(materialType);
    }
    
    public float getShineDamper() {
		return shineDamper;
	}

	public void setShineDamper(float shineDamper) {
		this.shineDamper = shineDamper;
	}

	public float getReflectivity() {
		return reflectivity;
	}

	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}
    
    private void getMatarialAttributes(MaterialLightReflectionType materialType) {
    	switch(materialType) {
	    	case ALUMINIUM: {
				this.shineDamper = 100;
				this.reflectivity = 20;
	    	}
	    	break;
	    	case CHROME: {
				this.shineDamper = 50;
				this.reflectivity = 1;
			}
	    	break;
    		case COOPER: {
    			this.shineDamper = 30;
    			this.reflectivity = 1;
    		}
    		break;
    		case BRICK: {
    			this.shineDamper = 10;
    			this.reflectivity = 0;
    		}
    		break;
    		case CARTOON: {
    			this.shineDamper = 0;
    			this.reflectivity = 0;
    		}
    		break;
    		case DEFAULT:
    		default: {
    			this.shineDamper = 1;
    			this.reflectivity = 0;
    		}
    		break;
    	}
    }
    
}
