package entities;

public enum MaterialLightReflectionType {
	DEFAULT,
	ALUMINIUM,
	CHROME,
	COOPER,
	BRICK,
	CARTOON
}
