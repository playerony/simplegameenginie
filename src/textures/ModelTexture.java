package textures;

import entities.Material;
import entities.MaterialLightReflectionType;

public class ModelTexture extends Material {
     
    private int textureID;
    
    private boolean hasTransparency = false;
     
    public ModelTexture(int texture){
    	super();
        this.textureID = texture;
    } 
    
    public boolean isHasTransparency() {
		return hasTransparency;
	}

	public void setHasTransparency(boolean hasTransparency) {
		this.hasTransparency = hasTransparency;
	}

	public ModelTexture(int texture, MaterialLightReflectionType materialType){
    	super(materialType);
        this.textureID = texture;
    }
     
    public int getID(){
        return textureID;
    }
 
}