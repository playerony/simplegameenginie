package water;

import java.io.Console;

import org.lwjgl.util.vector.Vector3f;

import entities.Entity;
import models.TexturedModel;

public class WaterEntity extends Entity {
	public static final float WATER_RANGE = 2;
	public static final float ANGLE_RANGE = 10;
	public static final float ZERO_LEVEL = -40;
	private float speed = 0.005f;
	private float angleSpeed = 0.005f;
	
	public WaterEntity(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super(model, position, rotX, rotY, rotZ, scale);
	}

	public void move() {
		if(super.getPosition().y - ZERO_LEVEL > WATER_RANGE)
			speed = -0.02f;
		
		if(super.getPosition().y - ZERO_LEVEL < -WATER_RANGE)
			speed = 0.02f;
		
		if(super.getRotY() > ANGLE_RANGE)
			angleSpeed = -0.005f;
		
		if(super.getRotY() < -ANGLE_RANGE)
			angleSpeed = 0.005f;
			
		super.increasePosition(0, speed, 0);
		super.increaseRotation(0, angleSpeed, 0);
	}
	
}
